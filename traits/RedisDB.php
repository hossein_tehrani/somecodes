<?php

namespace Modules\Base\Traits;

use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

trait RedisDB
{

    public function setRedis($key, $value, $time = null)
    {
        if (is_array($value)) {
            $value = json_encode($value);
        }
        Redis::set($key, $value);
        $time != null ? Redis::expire($key, $time) : 0;
    }

    public function setCacheRedis($key, $value, $time)
    {
        $cacheDriver = config('cache')['default'];
        if ($cacheDriver != 'redis') {
            Redis::set($key, $value);
            $time != null ? Redis::expire($key, $time) : 0;
            return true;
        } else {
            cache::get($key) ? cache::forget($key) : 0;
            return Cache::add(
                $key,
                $value,
                $time
            );
        }
    }

    public function getCacheRedis($key)
    {
        $cacheDriver = config('cache')['default'];
        if ($cacheDriver != 'redis') {
            return Redis::get($key);
        } else {
            return Cache::get($key);
        }
    }

    public function getRedis($key)
    {
        $data = Redis::get($key);
        if (is_string($data) && is_array(json_decode($data, true))) {
            $data = json_decode($data);
        }
        return $data;
    }

    public function setExRedis($key, $time)
    {
        return $time != null ? Redis::expire($key, $time) : '';
    }

    public function setPipeRedis($prefix, array $data)
    {
        Redis::pipeline(function ($pipe) use ($prefix, $data) {
            foreach ($data as $key => $item) {
                $pipe->set($prefix . ':' . $key, $data[$key]);
            }
        });
    }
}
