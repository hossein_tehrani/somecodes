<?php

namespace App\Filters\Product;

use App\Filters\Filters;

class ProductBrandFilter extends Filters
{
    protected $filters = ['byStatus', 'byProductCategory'];

    protected function byStatus($value)
    {
        return $this->builder->where('active', $value);
    }

    protected function byProductCategory($value)
    {
        return $this->builder->where('product_category_id', $value);
    }
}
