<?php

namespace App\Filters\Product;

use App\Filters\Filters;
use App\Models\ProductBrand;
use App\Models\ProductCategory;
use App\Models\User;
use Illuminate\Http\Request;

class ProductFilter extends Filters
{
    protected $filters = ['byBrand', 'byStatus', 'showInApp', 'mostSells', 'byCategory'];

    protected function byBrand($brandId)
    {
        $brand = ProductBrand::find($brandId);
        if ($brand instanceof ProductBrand) {
            return $this->builder->where('product_brand_id', $brand->id);
        }
        return $this->builder;
    }

    protected function byCategory($categoryId)
    {
        return $this->builder->whereHas('productBrand', function ($query) use ($categoryId) {
            return $query->where('product_category_id', $categoryId);
        });
    }

    protected function byStatus($value)
    {
        return $this->builder->where('active', $value);
    }

    protected function showInApp($value)
    {
        return $this->builder->where('show_in_app', $value);
    }

    protected function mostSells($value)
    {
        switch ($value) {
            case 'most_sells':
                $value = 'desc';
                break;
            case 'least_sells':
                $value = 'asc';
                break;
            default:
                $value = 'desc';
                break;
        }
        return $this->builder->orderBy('sells', $value);
    }
}
