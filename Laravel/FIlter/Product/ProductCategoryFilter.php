<?php

namespace App\Filters\Product;

use App\Filters\Filters;

class ProductCategoryFilter extends Filters
{
    protected $filters = ['byStatus'];

    protected function byStatus($value)
    {
        return $this->builder->where('active', $value);
    }
}
