<?php

namespace App\Filters\Product;

use App\Filters\Filters;

class ProductPropertyFilter extends Filters
{
    protected $filters = ['byPropertyCategory', 'byFilterable', 'byMultiple'];

    protected function byPropertyCategory($value)
    {
        return $this->builder->where('property_category_id', $value);
    }

    protected function byFilterable($value)
    {
        return $this->builder->where('filterable', $value);
    }

    protected function byMultiple($value)
    {
        return $this->builder->where('multiple', $value);
    }
}
