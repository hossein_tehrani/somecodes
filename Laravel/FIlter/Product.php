<?php

namespace App\Models;

use App\Filters\Product\ProductFilter;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Pishran\LaravelPersianSlug\HasPersianSlug;
use Spatie\Sluggable\SlugOptions;

class Product extends Model
{
    use HasFactory, HasPersianSlug, SoftDeletes;

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')
            ->slugsShouldBeNoLongerThan(100);
    }

    protected $guarded = ['id'];

    protected $appends = ['like'];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->where('status', 'accepted');
    }

    public function scopeFilter($query)
    {
        return (new ProductFilter())->apply($query);
    }

    public function getLikeAttribute()
    {
        if (auth()->check()) {
        return UserFavorite::where([
            ['product_id', $this->id],
            ['user_id', 2]
        ])->count();
        }
        return false;
    }
}
