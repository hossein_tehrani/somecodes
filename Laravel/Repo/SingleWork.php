<?php

namespace App\Http\Repo\TaskDB;

use App\Events\NotifyMultiMedia;
use App\Events\OrderCreated;
use App\Models\Admin;
use App\Models\ConfigAdmin;
use App\Models\Label;
use App\Models\Salary;
use App\Models\SmsConfig;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Modules\Product\Entities\Order;
use Modules\Product\Entities\OrderDetail;
use Modules\Product\Repo\OrderDB;
use Helpers\UploadFile;
use Morilog\Jalali\Jalalian;

class SingleWork extends Component
{
    use UploadFile;

    public $salary, $admin, $order = [], $comment, $current_desk, $labels, $datePicker = [], $temp = false, $counter = [],
        $persian_editorial_staff, $media_helper_staff, $english_editorial_staff, $multi_media_staff, $editor_staff, $translator_staff, $multi_media_editor_staff,
        $translator, $english_editor, $multimediaStaff = [],
        $graphist_staff, $englishStaff, $staff, $progress = 0;

    /* 
    * Prepearing data which contains tasks to do and staffs. 
    */
    public function bootstrap($task, $desk)
    {
        $id = Auth::id();
        $this->order['delivery'] = 'normal';
        $this->order['sensitivity'] = 'normal';
        $this->admin = Admin::with('roles')->findorfail($id);
        $this->multimediaStaff['editor'] = [];
        $this->multimediaStaff['translator'] = [];
        $this->englishStaff['editor'] = [];
        $this->englishStaff['translator'] = [];
        $this->media_helper_staff = Admin::whereHas('roles', function ($query) {
            $query->where('name', 'media_helper');
        })->get();
        $this->persian_editorial_staff = Admin::whereHas('roles', function ($query) {
            $query->where('name', 'persian_editorial');
        })->get();
        $this->english_editorial_staff = Admin::whereHas('roles', function ($query) {
            $query->where('name', 'english_editorial');
        })->get();
        $this->editor_staff = Admin::whereHas('roles', function ($query) {
            $query->where('name', 'editor');
        })->get();
        $this->translator_staff = Admin::whereHas('roles', function ($query) {
            $query->where('name', 'translator');
        })->get();
        $this->multi_media_staff = Admin::whereHas('roles', function ($query) {
            $query->where('name', 'multi_media');
        })->get();
        $this->multi_media_editor_staff = Admin::whereHas('roles', function ($query) {
            $query->where('name', 'multi_media_editor');
        })->get();
        $this->graphist_staff = Admin::whereHas('roles', function ($query) {
            $query->where('name', 'graphist');
        })->get();
        $this->order['rate'] = null;
        $this->comment = '';
        $this->current_desk = $desk;
        $this->admin = Auth()->guard('admin')->user();
        $this->order['orderDetail'] = OrderDetail::with('order')->findorfail($task);
        $this->order['order'] = Order::with('labels', 'publishChannel', 'userPlan')->findorfail($this->order['orderDetail']['order']['id']);
        $this->order['old']['order'] = OrderDetail::where('order_id', $this->order['order']['id'])->WhereHas('workflow', function ($query) {
            $query->where('name', 'persian');
        })->get()->last();
        if (isset($this->order['old']['order'])) {
            $this->order['old']['order'] = $this->order['old']['order']->toArray();
        }
        $this->order['comments'] = $this->order['order']->allComments();
        $this->order['user_comments'] = $this->order['order']->userComments();
        $this->order['labels'] = $this->order['order']['labels'];
        $this->labels = $this->order['order']['labels'];
        switch ($this->order['order']['type']) {
            case 'news':
                $this->order['title'] = $this->order['orderDetail']['title'];
                $this->order['lead'] = $this->order['orderDetail']['lead'];
                $this->order['content'] = $this->order['orderDetail']['content'];
                $this->counter['title'] = str_word_count($this->order['title']);
                $this->counter['lead'] = str_word_count($this->order['lead']);
                $this->counter['content'] = str_word_count($this->order['content']);
                if (isset($this->order['old']['order'])) {
                    $this->order['old']['title'] = $this->order['old']['order']['title'];
                    $this->order['old']['lead'] = $this->order['old']['order']['lead'];
                    $this->order['old']['content'] = $this->order['old']['order']['content'];
                }
                break;
            case 'video':
                $this->order['video'] = $this->order['orderDetail']['video'];
                $this->order['subtitle'] = $this->order['orderDetail']['video_subtitle'];
                $this->order['content'] = $this->order['orderDetail']['content'];
                $this->counter['subtitle'] = str_word_count($this->order['subtitle']);
                if (isset($this->order['old']['order'])) {
                    $this->order['old']['subtitle'] = $this->order['old']['order']['video_subtitle'];
                }
                break;
            case 'photo':
                $this->order['photo'] = $this->order['orderDetail']['photo'];
                $this->order['content'] = $this->order['orderDetail']['content'];
                $this->counter['text_photo'] = str_word_count($this->order['content']);
                if (isset($this->order['old']['order'])) {
                    $this->order['old']['content'] = $this->order['old']['order']['content'];
                }
                break;
            case 'mail':
                $this->order['title'] = $this->order['orderDetail']['title'];
                $this->order['content'] = $this->order['orderDetail']['content'];
                $this->counter['title'] = str_word_count($this->order['title']);
                $this->counter['content'] = str_word_count($this->order['content']);
                $this->order['signature'] = $this->order['orderDetail']['signature'];
                if (isset($this->order['old']['order'])) {
                    $this->order['old']['title'] = $this->order['old']['order']['title'];
                    $this->order['old']['signature'] = $this->order['old']['order']['signature'];
                    $this->order['old']['content'] = $this->order['old']['order']['content'];
                }
                break;
        }
        $this->counter['sum'] = 0;
        $this->salary=ConfigAdmin::first()->per_word_english;
    }

    /* 
    * Distribute tasks in Desks 
    */
    public function assignToStaff($taskId, $adminId, $type)
    {
        switch ($type) {
            case 'persian_editorial':
                $orderDetailInstance = OrderDetail::find($taskId);
                $orderDetailInstance->update([
                    'head_admin_id' => $this->admin->id,
                    'editor_id' => $adminId,
                    'status' => 'editing'
                ]);
                $smsInstance = SmsConfig::where([
                    ['type', '=', 'kavenegar'],
                    ['active', '=', true],
                ])->first();
                $admin = Admin::findorfail($adminId);
                switch ($orderDetailInstance->order->delivery_time) {
                    case 'normal' :
                        $orderDetailInstance->order->delivery_time = 'معمولی';
                        break;
                    case 'urgent' :
                        $orderDetailInstance->order->delivery_time = 'فوری';
                        break;
                    case 'super_urgent' :
                        $orderDetailInstance->order->delivery_time = 'فوق فوری';
                        break;
                }
                switch ($orderDetailInstance->order->sensitivity) {
                    case 'normal' :
                        $orderDetailInstance->order->sensitivity = 'معمولی';
                        break;
                    case 'sensitive' :
                        $orderDetailInstance->order->sensitivity = 'حساس';
                        break;
                }
                if ($orderDetailInstance->order->delivery_time == 'فوق فوری') {
                    $message = $admin->name . ' گرامی' . ' کاری با اولویت زمانی ' . '"' . $orderDetailInstance->order->delivery_time . '"' . ' با حساسیت ' . '"' . $orderDetailInstance->order->sensitivity . '"' . ' در کارتابل شما قرار گرفت' . '
                    ' .
                        ' لطفا سریعا کارتابل خود را چک کنید' .
                        '

                ifpnews
                ';
                } else {
                    $message = $admin->name . ' گرامی' . ' کاری با اولویت زمانی ' . '"' . $orderDetailInstance->order->delivery_time . '"' . ' با حساسیت ' . '"' . $orderDetailInstance->order->sensitivity . '"' . ' در کارتابل شما قرار گرفت' . '

                ifpnews
                ';
                }

                $api = new \Kavenegar\KavenegarApi($smsInstance->api_key);
                $sender = $smsInstance->number;
                $api->Send($sender, [$admin->mobile], $message);
                redirect()->to('admin/desk/single/' . $taskId . '/' . $this->current_desk);
                break;
            case 'english_editorial' :
                $orderDetailInstance = OrderDetail::find($taskId);
                if (!count($this->englishStaff['editor']) || !count($this->englishStaff['translator'])) {
                    session()->flash('message', 'لطفا ادیتور و مترجم را با هم انتخاب کنید.');
                    break;
                } else {
                    $orderDetailInstance->update([
                        'head_admin_id' => $this->admin->id,
                        'editor_id' => $this->englishStaff['editor'][$taskId],
                        'translator_id' => $this->englishStaff['translator'][$taskId],
                        'status' => 'translating'
                    ]);
                    $smsInstance = SmsConfig::where([
                        ['type', '=', 'kavenegar'],
                        ['active', '=', true],
                    ])->first();
                    $editor = Admin::findorfail($this->englishStaff['editor'][$taskId]);
                    $translator = Admin::findorfail($this->englishStaff['translator'][$taskId]);
                    switch ($orderDetailInstance->order->delivery_time) {
                        case 'normal' :
                            $orderDetailInstance->order->delivery_time = 'معمولی';
                            break;
                        case 'urgent' :
                            $orderDetailInstance->order->delivery_time = 'فوری';
                            break;
                        case 'super_urgent' :
                            $orderDetailInstance->order->delivery_time = 'فوق فوری';
                            break;
                    }
                    switch ($orderDetailInstance->order->sensitivity) {
                        case 'normal' :
                            $orderDetailInstance->order->sensitivity = 'معمولی';
                            break;
                        case 'sensitive' :
                            $orderDetailInstance->order->sensitivity = 'حساس';
                            break;
                    }
                    /*EDITOR NOTIFICATION*/
                    $message_editor = $editor->name . ' گرامی' . ' کاری با اولویت زمانی ' . '"' . $orderDetailInstance->order->delivery_time . '"' . ' با حساسیت ' . '"' . $orderDetailInstance->order->sensitivity . '"' . ' در کارتابل مترجم قرار گرفته و پس از ترجمه وارد کارتابل شما خواهد شد' .
                        ''
                        . '
                    ' . 'ifpnews';
                    /*TRANSLATOR NOTIFICATION*/
                    if ($orderDetailInstance->order->delivery_time == 'فوق فوری') {
                        $message_translator = $translator->name . ' گرامی' . ' کاری با اولویت زمانی ' . '"' . $orderDetailInstance->order->delivery_time . '"' . ' با حساسیت ' . '"' . $orderDetailInstance->order->sensitivity . '"' . ' در کارتابل شما قرار گرفت' . '
                    ' .
                            ' لطفا سریعا کارتابل خود را چک کنید' .
                            '

                ifpnews
                ';
                    } else {
                        $message_translator = $translator->name . ' گرامی' . ' کاری با اولویت زمانی ' . '"' . $orderDetailInstance->order->delivery_time . '"' . ' با حساسیت ' . '"' . $orderDetailInstance->order->sensitivity . '"' . ' در کارتابل شما قرار گرفت' . '

                ifpnews
                ';
                    }

                    $api = new \Kavenegar\KavenegarApi($smsInstance->api_key);
                    $sender = $smsInstance->number;
                    $api->Send($sender, [$translator->mobile], $message_translator);
                    $api->Send($sender, [$editor->mobile], $message_editor);
                    redirect()->to('admin/desk/single/' . $taskId . '/' . $this->current_desk);
                    break;
                }
            case 'multi_media_video' :
                $orderDetailInstance = OrderDetail::find($taskId);
                $orderDetailInstance->update([
                    'head_admin_id' => $this->admin->id,
                    'editor_id' => $this->multimediaStaff['editor'][array_key_first($this->multimediaStaff['editor'])],
                    'status' => 'editing'
                ]);
                $smsInstance = SmsConfig::where([
                    ['type', '=', 'kavenegar'],
                    ['active', '=', true],
                ])->first();
                $admin = Admin::findorfail($this->multimediaStaff['editor'][array_key_first($this->multimediaStaff['editor'])]);
                switch ($orderDetailInstance->order->delivery_time) {
                    case 'normal' :
                        $orderDetailInstance->order->delivery_time = 'معمولی';
                        break;
                    case 'urgent' :
                        $orderDetailInstance->order->delivery_time = 'فوری';
                        break;
                    case 'super_urgent' :
                        $orderDetailInstance->order->delivery_time = 'فوق فوری';
                        break;
                }
                switch ($orderDetailInstance->order->sensitivity) {
                    case 'normal' :
                        $orderDetailInstance->order->sensitivity = 'معمولی';
                        break;
                    case 'sensitive' :
                        $orderDetailInstance->order->sensitivity = 'حساس';
                        break;
                }
                if ($orderDetailInstance->order->delivery_time == 'فوق فوری') {
                    $message = $admin->name . ' گرامی' . ' کاری با اولویت زمانی ' . '"' . $orderDetailInstance->order->delivery_time . '"' . ' با حساسیت ' . '"' . $orderDetailInstance->order->sensitivity . '"' . ' در کارتابل شما قرار گرفت' . '
                    ' .
                        ' لطفا سریعا کارتابل خود را چک کنید' .
                        '

                ifpnews
                ';
                } else {
                    $message = $admin->name . ' گرامی' . ' کاری با اولویت زمانی ' . '"' . $orderDetailInstance->order->delivery_time . '"' . ' با حساسیت ' . '"' . $orderDetailInstance->order->sensitivity . '"' . ' در کارتابل شما قرار گرفت' . '

                ifpnews
                ';
                }

                $api = new \Kavenegar\KavenegarApi($smsInstance->api_key);
                $sender = $smsInstance->number;
                $api->Send($sender, [$admin->mobile], $message);
                redirect()->to('admin/desk/single/' . $taskId . '/' . $this->current_desk);
                break;
            case 'multi_media_photo' :
                $orderDetailInstance = OrderDetail::find($taskId);
                $orderDetailInstance->update([
                    'head_admin_id' => $this->admin->id,
                    'translator_id' => $this->multimediaStaff['translator'][array_key_first($this->multimediaStaff['translator'])],
                    'status' => 'translating'
                ]);
                $smsInstance = SmsConfig::where([
                    ['type', '=', 'kavenegar'],
                    ['active', '=', true],
                ])->first();
                $admin = Admin::findorfail($this->multimediaStaff['translator'][array_key_first($this->multimediaStaff['translator'])]);
                switch ($orderDetailInstance->order->delivery_time) {
                    case 'normal' :
                        $orderDetailInstance->order->delivery_time = 'معمولی';
                        break;
                    case 'urgent' :
                        $orderDetailInstance->order->delivery_time = 'فوری';
                        break;
                    case 'super_urgent' :
                        $orderDetailInstance->order->delivery_time = 'فوق فوری';
                        break;
                }
                switch ($orderDetailInstance->order->sensitivity) {
                    case 'normal' :
                        $orderDetailInstance->order->sensitivity = 'معمولی';
                        break;
                    case 'sensitive' :
                        $orderDetailInstance->order->sensitivity = 'حساس';
                        break;
                }
                if ($orderDetailInstance->order->delivery_time == 'فوق فوری') {
                    $message = $admin->name . ' گرامی' . ' کاری با اولویت زمانی ' . '"' . $orderDetailInstance->order->delivery_time . '"' . ' با حساسیت ' . '"' . $orderDetailInstance->order->sensitivity . '"' . ' در کارتابل شما قرار گرفت' . '
                    ' .
                        ' لطفا سریعا کارتابل خود را چک کنید' .
                        '

                ifpnews
                ';
                } else {
                    $message = $admin->name . ' گرامی' . ' کاری با اولویت زمانی ' . '"' . $orderDetailInstance->order->delivery_time . '"' . ' با حساسیت ' . '"' . $orderDetailInstance->order->sensitivity . '"' . ' در کارتابل شما قرار گرفت' . '

                ifpnews
                ';
                }

                $api = new \Kavenegar\KavenegarApi($smsInstance->api_key);
                $sender = $smsInstance->number;
                $api->Send($sender, [$admin->mobile], $message);
                redirect()->to('admin/desk/single/' . $taskId . '/' . $this->current_desk);
                break;
        }
    }

    public function updated()
    {
        switch ($this->order['order']['type']) {
            case 'news' :
                $this->counter['title'] = str_word_count($this->order['title']);
                $this->counter['content'] = str_word_count($this->order['content']);
                $this->counter['lead'] = str_word_count($this->order['lead']);
                $this->counter['sum'] = ($this->counter['title'] + $this->counter['content'] + $this->counter['lead']);

                break;
            case 'video':
                $this->counter['subtitle'] = str_word_count($this->order['subtitle']);
                break;
            case 'photo':
                $this->counter['text_photo'] = str_word_count($this->order['content']);
                break;
            case 'mail':
                $this->counter['title'] = str_word_count($this->order['title']);
                $this->counter['content'] = str_word_count($this->order['content']);
                break;
        }

    }

    /* 
    * estimate quality of staff 
    */
    public function rate($id)
    {
        OrderDetail::findorfail($id)->update([
            'rate' => $this->rate[$id]
        ]);
        session()->flash('message', 'ارزیابی شما با نمره ' . $this->rate[$id] . ' ثبت گردید');
    }

    public function datePicker($id, $i, $type)
    {
        if ($i <= 9 && $i >= 1) {
            $i = '0' . $i;
        }
        $this->order['duoDate'][$type][$id] = $i;
        $this->datePicker[$type] = $i;
    }

    public function SetDueTime($id, $type)
    {
        if (!isset($this->order['duoTime']['hour'][$id])) {
            $this->order['duoTime']['hour'][$id] = '0';
        }
        if (!isset($this->order['duoTime']['minute'][$id])) {
            $this->order['duoTime']['minute'][$id] = '0';
        }
        if ($this->order['duoTime']['hour'][$id] <= 9 && $this->order['duoTime']['hour'][$id] >= 0) {
            $hour = '0' . $this->order['duoTime']['hour'][$id];
        } else {
            $hour = $this->order['duoTime']['hour'][$id];
        }
        if ($this->order['duoTime']['minute'][$id] <= 9 && $this->order['duoTime']['minute'][$id] >= 0) {
            $minute = '0' . $this->order['duoTime']['minute'][$id];
        } else {
            $minute = $this->order['duoTime']['minute'][$id];
        }
        $this->order['duoTime'][$id] = $hour . ':' . $minute . ':' . '00';

        if (!isset($this->order['duoDate']['year'][$id])) {
            $this->order['duoDate']['year'][$id] = '1400';
        }
        if (!isset($this->order['duoDate']['day'][$id]) || !isset($this->order['duoDate']['month'][$id])) {
            session()->flash('error', 'روز و ماه را انتخاب کنید');
        } else {
            $date = $this->order['duoDate']['day'][$id] . '/' . $this->order['duoDate']['month'][$id] . '/' . $this->order['duoDate']['year'][$id];
            $this->order['duoDate']['full'][$id] = $date == '' ? '1400/01/01' : Jalalian::fromFormat('d/m/Y', $date)->toCarbon();
            if ($type == 'manager') {
                OrderDetail::findorfail($id)->update([
                    'deadline_date' => $this->order['duoDate']['full'][$id],
                    'deadline_time' => $this->order['duoTime'][$id],
                ]);
            } else {
                OrderDetail::findorfail($id)->update([
                    'deadline_date_2' => $this->order['duoDate']['full'][$id],
                    'deadline_time_2' => $this->order['duoTime'][$id],
                ]);
            }

            session()->flash('success', 'زمان بندی با موفقیت انجام شد');
        }
    }

    /* 
    * submit a task to next level 
    */
    public function submit($id, $type)
    {
        $order_id = OrderDetail::findorfail($id)->order_id;
        $order = Order::findorfail($order_id);
        $order->update([
            'delivery_time_admin' => $this->order['delivery'],
            'sensitivity_admin' => $this->order['sensitivity'],
        ]);
        if ($this->comment == false || $this->comment == "" || $this->comment == " ") {
            session()->flash('message', 'ارسال کامنت اجباری است!');
        } else {
            switch ($type) {
                case 'text' :
                    (new OrderDB)->continueOrder(
                        $id, $this->admin->id, [
                            'content' => $this->order['content'],
                            'title' => $this->order['title'],
                            'lead' => $this->order['lead'],
                        ]
                    );
                    if ($this->current_desk == 'persian_editorial') {
                        event(new OrderCreated($order, 'submit'));
                    }
                    (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
                    session()->flash('success', 'تایید سفارش با موفیقت انجام شد');
                    return redirect()->to('admin/desk/' . $this->current_desk);
                case 'video' :
                    (new OrderDB)->continueOrder(
                        $id, $this->admin->id, [
                            'content' => $this->order['content'],
                            'video' => $this->order['video'],
                            'video_subtitle' => $this->order['subtitle'],
                        ]
                    );
                    if ($this->current_desk == 'persian_editorial') {
                        event(new OrderCreated($order, 'submit'));
                    }
                    (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
                    session()->flash('success', 'تایید سفارش با موفیقت انجام شد');
                    return redirect()->to('admin/desk/' . $this->current_desk);
                case 'photo' :
                    if (isset($this->order['finalPhoto'])) {
                        if (isset($this->order['finalPhoto'])) {
                            $imageName = rand(1, 99999) . 'TaskId=' . $id . '-' . time() . '.' . $this->order['finalPhoto']->getClientOriginalExtension();
                            $this->order['picPath'] = '/storage/' . $this->order['finalPhoto']->storeAs(
                                    'orders', $imageName
                                );
                            session()->flash('message', 'عکس مورد نظر بارگزاری شد');
                        }
                        (new OrderDB)->continueOrder(
                            $id, $this->admin->id, [
                                'content' => $this->order['content'],
                                'photo' => $this->order['picPath'],
                            ]
                        );
                        if ($this->current_desk == 'persian_editorial') {
                            event(new OrderCreated($order, 'submit'));
                        }
                        (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
                        session()->flash('success', 'تایید سفارش با موفیقت انجام شد');
                        return redirect()->to('admin/desk/' . $this->current_desk);
                    } else {
                        (new OrderDB)->continueOrder(
                            $id, $this->admin->id, [
                                'content' => $this->order['content'],
                                'photo' => $this->order['photo'],
                            ]
                        );
                        if ($this->current_desk == 'persian_editorial') {
                            event(new OrderCreated($order, 'submit'));
                        }
                        (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
                        session()->flash('success', 'تایید سفارش با موفیقت انجام شد');
                        return redirect()->to('admin/desk/' . $this->current_desk);
                    }
                case 'mail' :
                    (new OrderDB)->continueOrder(
                        $id, $this->admin->id, [
                            'content' => $this->order['content'],
                            'title' => $this->order['title'],
                            'signature' => $this->order['signature'],
                        ]
                    );
                    if ($this->current_desk == 'persian_editorial') {
                        event(new OrderCreated($order, 'submit'));
                    }
                    (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
                    session()->flash('success', 'تایید سفارش با موفیقت انجام شد');
                    return redirect()->to('admin/desk/' . $this->current_desk);
            }
        }
    }

    /* 
    * Reject a task to previous level 
    */
    public
    function reject($id)
    {
        if ($this->comment == false || $this->comment == "" || $this->comment == " ") {
            session()->flash('message', 'ارسال کامنت اجباری است!');
        } else {
            (new OrderDB)->rejectOrder($id, $this->admin->id);
            (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
            session()->flash('message', 'سفارش با موفقیت لغو شد');
            return redirect()->to('admin/desk/' . $this->current_desk);
        }
    }

    /* 
    * Return task to customer with reason of reject 
    */
    public
    function passToCustomer($id, $type)
    {
        if ($this->comment == false || $this->comment == "" || $this->comment == " ") {
            session()->flash('message', 'ارسال کامنت اجباری است!');
        } else {
            switch ($type) {
                case 'news':
                    (new OrderDB)->rejectOrderToUser(
                        $id, $this->admin->id, ['content' => $this->order['content'],
                            'title' => $this->order['title'],
                            'lead' => $this->order['lead'],
                        ]
                    );
                    break;
                case 'mail':
                    (new OrderDB)->rejectOrderToUser(
                        $id, $this->admin->id, ['content' => $this->order['content'],
                            'title' => $this->order['title'],
                            'signature' => $this->order['signature'],
                        ]
                    );
                    break;
                case 'video' :
                    (new OrderDB)->rejectOrderToUser(
                        $id, $this->admin->id, [
                            'video' => $this->order['video'],
                            'content' => $this->order['content'],
                            'video_subtitle' => $this->order['subtitle'],
                        ]
                    );
                    (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
                    session()->flash('message', 'سفارش با موفقیت به اتمام رسید');
                    return redirect()->to('admin/desk/' . $this->current_desk);
                case 'photo' :
                    if (isset($this->order['finalPhoto'])) {
                        if (isset($this->order['finalPhoto'])) {
                            $imageName = rand(1, 99999) . 'TaskId=' . $id . '-' . time() . '.' . $this->order['finalPhoto']->getClientOriginalExtension();
                            $this->order['picPath'] = '/storage/' . $this->order['finalPhoto']->storeAs(
                                    'orders', $imageName
                                );
                            session()->flash('message', 'عکس مورد نظر بارگزاری شد');
                        }
                        (new OrderDB)->rejectOrderToUser(
                            $id, $this->admin->id, [
                                'content' => $this->order['content'],
                                'photo' => $this->order['picPath'],
                            ]
                        );
                    } else {
                        (new OrderDB)->rejectOrderToUser(
                            $id, $this->admin->id, [
                                'content' => $this->order['content'],
                                'photo' => $this->order['photo'],
                            ]
                        );
                    }

            }
            (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
            session()->flash('message', 'سفارش با موفقیت به کاربر عودت داده شد');
            return redirect()->to('admin/desk/' . $this->current_desk);
        }
    }

    /* 
    * Last level of Work Flow, finish the order and pass the result successfully to customer 
    */
    public
    function finish($id, $type)
    {
        if ($this->comment == false || $this->comment == "" || $this->comment == " ") {
            session()->flash('message', 'ارسال کامنت اجباری است!');
        } else {
            $orderDetail = OrderDetail::findorfail($id);
            if (isset($this->order['rate'])) {
                $orderDetail->update([
                    'rate' => $this->order['rate']
                ]);
            }
            $order = Order::findorfail($orderDetail->order_id);
            $salary = Salary::where('order_id', $order->id)->first();
            if ($salary) {
                $salary->update([
                    'accept' => true
                ]);
            } else {
                Salary::create([
                        'admin_id' => $this->admin->id,
                        'order_id' => $order->id,
                        'count_word' => $this->counter['sum'],
                        'accept' => true,
                    ]
                );
            }
            switch ($type) {
                case 'text' :
                    (new OrderDB)->finishOrder(
                        $id, $this->admin->id, [
                            'content' => $this->order['content'],
                            'title' => $this->order['title'],
                            'lead' => $this->order['lead'],
                        ]
                    );
                    event(new OrderCreated($order, 'finish'));
                    (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
                    session()->flash('message', 'سفارش با موفقیت به اتمام رسید');
                    return redirect()->to('admin/desk/' . $this->current_desk);
                case 'mail' :
                    (new OrderDB)->finishOrder(
                        $id, $this->admin->id, [
                            'content' => $this->order['content'],
                            'title' => $this->order['title'],
                            'signature' => $this->order['signature'],
                        ]
                    );
                    event(new OrderCreated($order, 'finish'));
                    (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
                    session()->flash('message', 'سفارش با موفقیت به اتمام رسید');
                    return redirect()->to('admin/desk/' . $this->current_desk);
                case 'video' :
                    (new OrderDB)->finishOrder(
                        $id, $this->admin->id, [
                            'video' => $this->order['video'],
                            'content' => $this->order['content'],
                            'video_subtitle' => $this->order['subtitle'],
                        ]
                    );
                    event(new OrderCreated($order, 'finish'));
                    (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
                    session()->flash('message', 'سفارش با موفقیت به اتمام رسید');
                    return redirect()->to('admin/desk/' . $this->current_desk);
                case 'photo' :
                    if (isset($this->order['finalPhoto'])) {
                        if (isset($this->order['finalPhoto'])) {
                            $imageName = rand(1, 99999) . 'TaskId=' . $id . '-' . time() . '.' . $this->order['finalPhoto']->getClientOriginalExtension();
                            $this->order['picPath'] = '/storage/' . $this->order['finalPhoto']->storeAs(
                                    'orders', $imageName
                                );
                            session()->flash('message', 'عکس مورد نظر بارگزاری شد');
                        }
                        (new OrderDB)->finishOrder(
                            $id, $this->admin->id, [
                                'content' => $this->order['content'],
                                'photo' => $this->order['picPath'],
                            ]
                        );
                        event(new OrderCreated($order, 'finish'));
                        (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
                        session()->flash('message', 'سفارش با موفقیت به اتمام رسید');
                        return redirect()->to('admin/desk/' . $this->current_desk);
                    } else {
                        (new OrderDB)->finishOrder(
                            $id, $this->admin->id, [
                                'content' => $this->order['content'],
                                'photo' => $this->order['photo'],
                            ]
                        );
                    }
                    event(new OrderCreated($order, 'finish'));
                    (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
                    session()->flash('message', 'سفارش با موفقیت به اتمام رسید');
                    return redirect()->to('admin/desk/' . $this->current_desk);
            }
        }
    }

    public
    function comment($id)
    {
        (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
        session()->flash('message', 'کامنت شما  با موفقیت ثبت شد');
        return redirect()->to('admin/desk/' . $this->current_desk);
    }

    /* 
    * Update every task in workflow (condition by ACL) 
    */
    public function edit($id, $type)
    {
        if ($this->comment == false || $this->comment == "" || $this->comment == " ") {
            session()->flash('message', 'ارسال کامنت اجباری است!');
        } else {
            if (isset($this->order['rate'])) {
                OrderDetail::findorfail($id)->update([
                    'rate' => $this->order['rate']
                ]);
            }
            $orderDetail = OrderDetail::findorfail($id);
            $order = Order::findorfail($orderDetail->order_id);
            if (isset($orderDetail['translator_id'])) {
                $translator = Admin::findorfail($orderDetail['translator_id']);
            } else {
                $translator = $this->admin;
            }
            if ($this->current_desk == 'english_editorial' && $this->admin->roles->pluck('name')->contains('translator')) {
                $order_id = OrderDetail::find($id)->order_id;
                Salary::create([
                        'admin_id' => $this->admin->id,
                        'order_id' => $order_id,
                        'count_word' => $this->counter['sum'],
                    ]
                );
            }
            switch ($type) {
                case 'text' :
                    (new OrderDB)->editTask(
                        $id, $this->admin->id, [
                            'content' => $this->order['content'],
                            'title' => $this->order['title'],
                            'lead' => $this->order['lead'],
                        ]
                    );
                    (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
                    session()->flash('message', 'سفارش با موفقیت ویرایش شد');
                    return redirect()->to('admin/desk/' . $this->current_desk);
                case 'mail' :
                    (new OrderDB)->editTask(
                        $id, $this->admin->id, [
                            'content' => $this->order['content'],
                            'title' => $this->order['title'],
                            'signature' => $this->order['signature'],
                        ]
                    );
                    (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
                    session()->flash('message', 'سفارش با موفقیت ویرایش شد');
                    return redirect()->to('admin/desk/' . $this->current_desk);
                case 'video' :
                    (new OrderDB)->editTask(
                        $id, $this->admin->id, [
                            'video' => $this->order['video'],
                            'content' => $this->order['content'],
                            'video_subtitle' => $this->order['subtitle'],
                        ]
                    );
                    (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
                    session()->flash('message', 'سفارش با موفقیت ویرایش شد');
                    return redirect()->to('admin/desk/' . $this->current_desk);
                case 'photo' :
                    if (isset($this->order['finalPhoto'])) {
                        if (isset($this->order['finalPhoto'])) {
                            $imageName = rand(1, 99999) . 'TaskId=' . $id . '-' . time() . '.' . $this->order['finalPhoto']->getClientOriginalExtension();
                            $this->order['picPath'] = '/storage/' . $this->order['finalPhoto']->storeAs(
                                    'orders', $imageName
                                );
                            session()->flash('message', 'عکس مورد نظر بارگزاری شد');
                        }
                        (new OrderDB)->editTask(
                            $id, $this->admin->id, [
                                'content' => $this->order['content'],
                                'photo' => $this->order['picPath'],
                            ]
                        );
                    } else {
                        (new OrderDB)->editTask(
                            $id, $this->admin->id, [
                                'content' => $this->order['content'],
                                'photo' => $this->order['photo'],
                            ]
                        );
                    }
                    (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
                    session()->flash('message', 'سفارش با موفقیت ویرایش شد');
                    return redirect()->to('admin/desk/' . $this->current_desk);
            }
        }
    }

    /* 
    * Reject the task to previous level - by editor to translator
    */
    public
    function rejectToTranslator($id, $type)
    {
        if ($this->comment == false || $this->comment == "" || $this->comment == " ") {
            session()->flash('message', 'ارسال کامنت اجباری است!');
        } else {
            if (isset($this->order['rate'])) {
                OrderDetail::findorfail($id)->update([
                    'rate' => $this->order['rate']
                ]);
            }
            switch ($type) {
                case 'text' :
                    (new OrderDB)->rejectToTranslator(
                        $id, $this->admin->id, ['content' => $this->order['content'],
                            'title' => $this->order['title'],
                            'lead' => $this->order['lead'],
                        ]
                    );
                    session()->flash('message', 'کار با موفقیت به مترجم بازگردانده شد');
                    break;
                case 'photo' :
                    (new OrderDB)->rejectToTranslator(
                        $id, $this->admin->id, [
                            'content' => $this->order['content'],
                            'photo' => $this->order['picPath'],
                        ]
                    );
                    session()->flash('message', 'کار با موفقیت به مترجم بازگردانده شد');
                    break;
                case 'video' :
                    (new OrderDB)->rejectToTranslator(
                        $id, $this->admin->id, [
                            'video' => $this->order['video'],
                            'content' => $this->order['content'],
                            'video_subtitle' => $this->order['subtitle'],
                        ]
                    );
                    session()->flash('message', 'کار با موفقیت به مترجم بازگردانده شد');
                    break;
            }
            (new OrderDB)->addComment($id, $this->admin->id, $this->comment);
            return redirect()->to('admin/desk/' . $this->current_desk);
        }
    }

    public
    function tag($id)
    {
        if (isset($this->order['labels']['label']) && isset($this->order['labels']['description'])) {
            if (!isset($this->order['labels']['level'])) {
                $this->order['labels']['level'] = 'danger';
            }
            $order = Order::findorfail($id);
            $label = new Label([
                'name' => $this->order['labels']['label'],
                'icon' => $this->order['labels']['level'],
                'order_id' => $id,
                'description' => $this->order['labels']['description'],
            ]);
            $this->labels[] = $label;
            $order->labels()->save($label);
        } else {
            session()->flash('message', 'لطفا ورودی ها را کنترل کنید');
        }
    }
}
